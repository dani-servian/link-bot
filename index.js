const linkedin = require('./linkedin');
const args = process.argv;

(async () => {
	await linkedin.initialize();
	await linkedin.login(args[2], args[3]);
	await linkedin.connect();
	process.exit(0);
})();
