const puppeteer = require('puppeteer');

const BASE_URL = 'https://www.linkedin.com';

const linkedin = {
	browser: null,
	page: null,

	initialize: async () => {
		linkedin.browser = await puppeteer.launch({
			headless: false
		});

		linkedin.page = await linkedin.browser.newPage();
	},

	login: async (user, pass) => {
		await linkedin.page.goto(BASE_URL, { waitUntil: 'networkidle2' });

		/* Get and click on the login button */
		let loginPageButton = await linkedin.page.$x('//a[contains(text(), "Inicia sesión")]');
		await loginPageButton[0].click();

		/* Navigate to login page */
		await linkedin.page.waitForNavigation({ waitUntil: 'networkidle2' });

		/* Wait for 1 second */
		await linkedin.page.waitFor(1000);

		/* Writing username and password */
		await linkedin.page.type('input[id="username"', user, { delay: 50 });
		await linkedin.page.type('input[id="password"]', pass, { delay: 50 });

		/* Get and click the login button*/
		let loginButton = await linkedin.page.$x('//button[contains(text(), "Iniciar sesión")]');
		await loginButton[0].click();

		/* Wait for login */
		await linkedin.page.waitFor(5000);
	},

	connect: async () => {
		/* Goto my network */
		await linkedin.page.goto(BASE_URL + '/mynetwork/', { waitUntil: 'networkidle2' });

		/* Scroll down 8 times */
		for (let i = 0; i < 8; i++) {
			await linkedin.page.evaluate(() => {
				window.scrollBy(0, window.innerHeight);
			});
			await linkedin.page.waitFor(1000);
		}

		await linkedin.page.waitFor(3000);

		/* Click on 50 connect buttons */
		let connectButtons = await linkedin.page.$$('button[data-control-name="invite"');
		for (let i = 0; i < 51; i++) {
			await connectButtons[i].click();
			await linkedin.page.waitFor(1000 + Math.random() * 200);
		}

		console.log('Successful');
	}
};

module.exports = linkedin;
